package com.jt.controller;

import com.jt.feign.RemoteProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/consumer/")
public class FeignConsumerController {
     @Autowired
     private RemoteProviderService remoteProviderService;
     //http://localhost:8090/consumer/echo/abcd
     @GetMapping("echo/{msg}")
     public String doFeignEcho(@PathVariable String msg){
         return remoteProviderService.echoMessage(msg);
     }
}
