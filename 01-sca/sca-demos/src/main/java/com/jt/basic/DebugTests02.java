package com.jt.basic;

public class DebugTests02 {
    public static void main(String[] args) {
        System.out.println("基于断点方式分析和测试整数池");
        Integer a1=100;
        Integer a2=100;//Integer.valueOf(100)
        Integer a3=200;//Integer.valueOf(200)
        Integer a4=200;
        System.out.println(a1==a2);//true
        System.out.println(a3==a4);//false

        Integer a5=new Integer(100);
        System.out.println(a1==a5);//false
        int a6=100;
        System.out.println(a5==a6);//true,此时a5会进行拆箱操作a5.intValue()
    }
}
