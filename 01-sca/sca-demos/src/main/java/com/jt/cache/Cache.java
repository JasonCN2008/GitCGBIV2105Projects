package com.jt.cache;

/**
 * Cache 接口规范设计
 */
public interface Cache {
    /**
     * 存储数据
     * @param key
     * @param value
     */
    void putObject(Object key,Object value);

    /**
     * 基于key获取数据
     * @param key
     * @return
     */
    Object getObject(Object key);

    /**
     * 移除指定key的数据
     * @param key
     * @return
     */
    Object removeObject(Object key);

    /**
     * 清空缓存
     */
    void clear();

    /**
     * 获取缓存中数据的个数
     * @return
     */
    int size();
    //...
}
