package com.jt.cache;

import java.util.HashMap;
import java.util.Map;

public class DefaultCache implements  Cache{
    private Map<Object,Object> cache=new HashMap<>();
    @Override
    public void putObject(Object key, Object value) {
         cache.put(key,value);
    }

    @Override
    public Object getObject(Object key) {
        return cache.get(key);
    }

    @Override
    public Object removeObject(Object key) {
        return cache.remove(key);
    }

    @Override
    public void clear() {
         cache.clear();
    }

    @Override
    public int size() {
        return cache.size();
    }

    @Override
    public String toString() {
        return "DefaultCache{" +
                "cache=" + cache.toString() +
                '}';
    }

    public static void main(String[] args) {
        Cache cache=new DefaultCache();
        cache.putObject("A",100);
        cache.putObject("B",200);
        cache.putObject("C",300);
        System.out.println(cache);
    }
}
