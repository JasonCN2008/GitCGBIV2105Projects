package com.jt.cache;

import java.util.Deque;
import java.util.LinkedList;

/**
 * 有界缓存淘汰策略:FIFO(先进先出)算法
 */
public class FifoCache implements Cache{
    /**存储数据的Cache*/
    private Cache cache;
    /**Cache的最大容量*/
    private int maxCap;
    /**双端队列(两头都可以操作),基于此队列记录key的顺序*/
    private Deque<Object> deque;
    public FifoCache(Cache cache, int maxCap) {
        this.cache = cache;
        this.maxCap = maxCap;
        this.deque=new LinkedList<>();
    }
    @Override
    public void putObject(Object key, Object value) {
        //1.记录key的顺序
        deque.addLast(key);
        //2.判定cache是否已满,满了则移除元素
        //if(cache.size()==maxCap){} 方式1
        if(deque.size()>maxCap){//方式2
            //获取最先放入的元素key
            Object eldestKey=deque.removeFirst();
            //移除最先放进去的元素
            cache.removeObject(eldestKey);
        }
        //3.添加新的元素
        cache.putObject(key,value);
    }
    @Override
    public Object getObject(Object key) {
        return cache.getObject(key);
    }

    @Override
    public Object removeObject(Object key) {
        Object value=cache.removeObject(key);
        deque.remove(key);
        return value;
    }

    @Override
    public void clear() {
        cache.clear();
        deque.clear();
    }

    @Override
    public int size() {
        return cache.size();
    }

    @Override
    public String toString() {
        return "FifoCache{" +
                "cache=" + cache +
                '}';
    }

    public static void main(String[] args) {
        FifoCache cache = new FifoCache(new DefaultCache(), 3);
        cache.putObject("A",100);
        cache.putObject("B",200);
        cache.putObject("C",300);
        cache.putObject("D",400);
        cache.putObject("E",500);
        System.out.println(cache);
    }
}
