package com.jt.container;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * 阻塞式队列
 */
public class BlockQueueTests {
    public static void main(String[] args) throws InterruptedException {
        //FIFO(先进先出-体现公平性):类似超市排队结账
        //1.对象构建
        BlockingQueue<Integer> queue=
                new ArrayBlockingQueue<Integer>(2);
                //new LinkedBlockingQueue<>(5);
        //2.存储数据
        queue.add(100);
        queue.add(200);
        System.out.println(queue);//100,200
        //queue.add(300);//抛出异常
        queue.put(300);//满了则阻塞

        //System.out.println(queue);//输出不了
        System.out.println(queue.remove());
        System.out.println(queue.remove());
        //System.out.println(queue.remove());//抛出异常
        Integer result=queue.take();//阻塞式方法,空则阻塞
        System.out.println("result="+result);

    }
}
