package com.jt.jdk5;

import java.util.concurrent.TimeUnit;

//枚举类型JDK5推出一种新的数据类型,主要用于定义一些固定值.
//一年四季
//一周七天
//订单状态
//性别要求
enum Gender{//Gender.class
    //定义三个枚举实例(类加载时创建)
    MALE,FEMALE,NONE("没有性别要求");
    private String name;
    private Gender(){}
    private Gender(String name){
        this.name=name;
    }
    public String getName(){
        return this.name;
    }
}
class Product{
    //性别要求
    Gender gender=Gender.NONE;
}

public class EnumTests {
    public static void main(String[] args) throws InterruptedException {
        String name = Gender.NONE.getName();
        System.out.println("name="+name);
        TimeUnit.SECONDS.sleep(3);
    }
}
