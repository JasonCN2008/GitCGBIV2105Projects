package com.jt.thread;
//J.U.C
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;

public class ThreadPoolTests {
    public static void main(String[] args) {
//         public ThreadPoolExecutor(int corePoolSize,
//        int maximumPoolSize,
//        long keepAliveTime,
//        TimeUnit unit,
//        BlockingQueue<Runnable> workQueue)
        //1.核心线程数
        int corePoolSize=2;
        //2.最大线程数
        int maximumPoolSize=3;
        //3.线程最大空闲时间
        long keepAliveTime=60;
        //4.时间单位(枚举-1.5)
        TimeUnit unit=TimeUnit.SECONDS;
        //5.任务队列(阻塞式队列)
        BlockingQueue<Runnable> workQueue=
                new ArrayBlockingQueue<>(1);

        //6.定义拒绝策略(可选，常用有四种)
        RejectedExecutionHandler handler=
                //当池无法处理这个任务时,由任务启动方去执行任务
                new ThreadPoolExecutor.CallerRunsPolicy();
        //7.构建线程工厂(可选,最关键是要给线程一个友好的名字)
        ThreadFactory factory=new ThreadFactory() {
            //线程名前缀
            private String namePrefix="cgb-2015-thread-";
            //构建一个线程安全的原子自增自减对象
            private AtomicLong atomicLong= new AtomicLong(1);
            @Override
            public Thread newThread(Runnable r) {//r 为任务
                return new Thread(r,namePrefix+atomicLong.getAndIncrement());
            }
        };
        //8.创建线程池
        ThreadPoolExecutor pool=
                new ThreadPoolExecutor(
                        corePoolSize,
                        maximumPoolSize,
                        keepAliveTime,
                        unit,
                        workQueue,
                        factory,
                        handler);
        //7.将任务交给池中对象去执行
        pool.execute(new Runnable() {
            @Override
            public void run() {
                String tName=
                Thread.currentThread().getName();
                System.out.println(tName+"->任务1");
                try{Thread.sleep(5000);}catch (Exception e){}
            }
        });
        pool.execute(new Runnable() {
            @Override
            public void run() {
                String tName=
                        Thread.currentThread().getName();
                System.out.println(tName+"->任务2");
                try{Thread.sleep(5000);}catch (Exception e){}
            }
        });
        pool.execute(new Runnable() {
            @Override
            public void run() {
                String tName=
                        Thread.currentThread().getName();
                System.out.println(tName+"->任务3");
                try{Thread.sleep(5000);}catch (Exception e){}
            }
        });
        pool.execute(new Runnable() {
            @Override
            public void run() {
                String tName=
                        Thread.currentThread().getName();
                System.out.println(tName+"->任务4");
                try{Thread.sleep(5000);}catch (Exception e){}
            }
        });
        pool.execute(new Runnable() {
            @Override
            public void run() {
                String tName=
                        Thread.currentThread().getName();
                System.out.println(tName+"->任务5");
            }
        });

        pool.shutdown();//不再接收任务,等现有任务执行结束将池关掉
       // pool.shutdownNow();//不再接收任务,但有可能会将现有任务也会停止
    }
}
