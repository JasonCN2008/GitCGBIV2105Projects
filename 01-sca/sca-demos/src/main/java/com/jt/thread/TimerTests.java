package com.jt.thread;

import java.util.Timer;
import java.util.TimerTask;

public class TimerTests {
    public static void main(String[] args) {
        //通过timer对象可以启动一个定时任务
        Timer timer=new Timer();
        //基于timer对象启动并执行任务
        timer.schedule(new TimerTask() {//这是一个任务对象
            @Override
            public void run() {
                System.out.println(System.currentTimeMillis());
            }
        },1000,//1秒以后开始执行
                1000);//每隔1秒执行1次


    }
}
