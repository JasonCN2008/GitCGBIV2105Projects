package com.jt.config;
import com.alibaba.csp.sentinel.adapter.gateway.sc.callback.GatewayCallbackManager;
import com.google.gson.Gson;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class GatewayConfig {
    public GatewayConfig(){
        //自定义限流结果
        GatewayCallbackManager.setBlockHandler((
                    serverWebExchange, throwable) ->{
                //构建响应数据
                Map<String,Object> map=new HashMap<>();
                map.put("state",429);
                map.put("message","two many request");
                //基于alibaba 的fastjson将对象转换为json
                //String jsonStr= JSON.toJSONString(map);//fastjson
                //基于Google公司的gson将对象转换为json
                Gson gson=new Gson();
                String jsonStr=gson.toJson(map);
                //创建Mono对象,将结果响应到客户端
                return ServerResponse.ok().body(Mono.just(jsonStr),
                        String.class);//String.class表示响应数据类型
            //WebFlux
        });
    }
}
