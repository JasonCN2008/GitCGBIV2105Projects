package com.jt;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@SpringBootApplication
public class ProviderApplication {
    //创建java中的日志对象(SLF4J是java中的日志规范,是日志对外的窗口,是门面)
    //目前市场上对SLF4J规范的实现主要有两种:log4j,logback
    private static final Logger log=
            LoggerFactory.getLogger(ProviderController.class);
    public static void main(String[] args) {
        SpringApplication.run(ProviderApplication.class,args);
    }

    /**定义Controller对象(这个对象在spring mvc中给他的定义是handler),
     * 基于此对象处理客户端的请求*/
    @RefreshScope//这个注解描述类时,当配置中心数据发生变化会对属性进行重新初始化
    @RestController
    public class ProviderController{

        public ProviderController(){
            System.out.println("==ProviderController()==");
        }

        //@Value默认读取项目配置文件中配置的内容
        //8080为没有读到server.port的值时,给定的默认值
        @Value("${server.port:8080}")
        private String server;
        //http://localhost:8081/provider/echo/tedu
        @GetMapping("/provider/echo/{msg}")
        public String doRestEcho1(@PathVariable String msg, HttpServletRequest request)
                throws InterruptedException {
            String header = request.getHeader("X-Request-Foo");
            String foo = request.getParameter("foo");
            // Thread.sleep(50000);
           // System.out.println("===");//耗时
            return server+" say hello "+msg;
        }

        @Value("${logging.level.com.jt:error}")
        private String logLevel;

        @GetMapping("/provider/doGetLogLevel")
        public String doGetLogLevel(){
            //日志级别trace<debug<info<warn<error
            log.trace("==log.trace==");//跟踪
            log.debug("==log.debug==");//调试
            log.info("==log.info==");//常规信息
            log.warn("==log.warn==");//警告
            log.error("==log.error==");//错误信息
            return "log level is "+logLevel;
        }

        @Value("${server.tomcat.threads.max:200}")
        private Integer maxThread;

        @RequestMapping("/provider/doGetMaxThread")
        public String doGetMaxThread(){
            return "server.threads.max is  "+maxThread;
        }

        @Value("${page.pageSize:10}")
        private Integer pageSize;

        @GetMapping("/provider/doGetPageSize")
        public String doGetPageSize(){
            //return String.format()
            return "page size is "+pageSize;
        }

    }
}
