package com.jt.resource.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Slf4j
@Aspect
@Component
public class LogAspect {
    @Pointcut("@annotation(com.jt.resource.annotation.RequiredLog)")
    public void doLog(){}//锦上添花的锦(注解描述的方法)

    @Around("doLog()")
    //@Around("@annotation(com.jt.resource.annotation.RequiredLog)")
    public Object doAround(ProceedingJoinPoint joinPoint) throws Throwable {

        log.debug("Before {}",System.currentTimeMillis());
        Object result=joinPoint.proceed();//执行执行链(其它切面,目标方法-锦)
        log.debug("After {}",System.currentTimeMillis());
        //后续获取登录用户信息
//        Object principal =
//                SecurityContextHolder.getContext()
//                        .getAuthentication().getPrincipal();
//        System.out.println("principal="+principal);
        //假如要获取目标方法信息可以通过joinPoint对象,去拿到目标方法以及实际参数
        //后续要封装日志信息,然后通过RemoteLogService将日志提交日志服务或系统服务
        return result;//目标方法(切入点方法)的执行结果
    }
}
