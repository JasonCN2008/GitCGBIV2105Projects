package com.jt.resource.controller;

import com.jt.resource.annotation.RequiredLog;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

/**
 * 通过此对象实现文件上传服务
 */
@Slf4j
//@CrossOrigin
@RefreshScope
@RestController
@RequestMapping("/resource/")
public class ResourceController {
      //当了类的上面添加了@Slf4J就不用自己创建下面的日志对象了
//    private static final Logger log=
//            LoggerFactory.getLogger(ResourceController.class);

     @Value("${jt.resource.path}")
     private String resourcePath;//="d:/uploads/";
     @Value("${jt.resource.host}")
     private String resourceHost;//="http://localhost:8881/";

     //将RequiredLog注解描述的方法作为切入点方法,在方法执行之前和之后进行日志记录
     //@PreAuthorize注解描述的方法表示必须已认证用户且有sys:res:create这个权限才可以访问
     @PreAuthorize("hasAuthority('sys:res:create')")
     @RequiredLog("文件上传") //扩展业务->日志记录
     @PostMapping("/upload/") //核心业务->文件上传
     public String uploadFile(MultipartFile uploadFile) throws IOException {
         //log.debug("Before {}",System.currentTimeMillis());
         //1.创建文件存储目录(按时间创建-yyyy/MM/dd)
         //1.1获取当前时间的一个目录
         String dateDir = DateTimeFormatter.ofPattern("yyyy/MM/dd")
                 .format(LocalDate.now());
         //1.2构建目录文件对象
         File uploadFileDir=new File(resourcePath,dateDir);
         if(!uploadFileDir.exists())uploadFileDir.mkdirs();
         //2.给文件起个名字(尽量不重复)
         //2.1获取原文件后缀
         String originalFilename=uploadFile.getOriginalFilename();
         String ext = originalFilename.substring(
                 originalFilename.lastIndexOf("."));
         //2.2构建新的文件名
         String newFilePrefix=UUID.randomUUID().toString();
         String newFileName=newFilePrefix+ext;
         //3.开始实现文件上传
         //3.1构建新的文件对象,指向实际上传的文件最终地址
         File file=new File(uploadFileDir,newFileName);
         //3.2上传文件(向指定服务位置写文件数据)
         uploadFile.transferTo(file);
         String fileRealPath=resourceHost+dateDir+"/"+newFileName;
         log.debug("fileRealPath {}",fileRealPath);
         //后续可以将上传的文件信息写入到数据库?
         return fileRealPath;
     }

}
