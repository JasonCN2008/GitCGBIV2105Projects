package com.jt.demos;

import redis.clients.jedis.Jedis;

/**登录验证码实践
 * 1)进入登录页面之前先创建一个验证码,并将其存储在redis中
 * 2)登录时输入验证码,并且将验证码与redis中的验证码进行比对
 * */
public class CodeDemo01 {
    static void doLogin(String username,String password,String inputCode){
       //1.校验验证码
        //1.1验证是否为空
       if(inputCode==null||"".equals(inputCode)){
           System.out.println("please input code");
           return;
       }
       //1.2验证redis中的code
       Jedis jedis=new Jedis("192.168.126.130",6379);
       String dbCode=jedis.get("code");
       if(dbCode==null){
           System.out.println("code timeout");
           return;
       }
       if(!inputCode.equals(dbCode)){
           System.out.println("code error");
           return;
       }
       //2......
       System.out.println("继续验证用户身份合法性");
    }
    public static void main(String[] args) {
        char[] chars={
                'A','B','C','D','1','2','.'
        };//后续可以写一个算法从这个数组中随机取出四个值
       //1.生成一个随机验证码
        String code="12AB";
        Jedis jedis=new Jedis("192.168.126.130",6379);
        jedis.set("code","12AB");
        jedis.expire("code",60);
        jedis.close();
       //2.执行登录操作
        doLogin("jack","123456","12AB");
    }
}
