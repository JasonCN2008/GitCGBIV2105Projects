package com.jt.redis;

import org.junit.Test;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class JedisPoolTests {
    @Test
    public void testJedisPool(){
        //1.初始化连接池的配置
        JedisPoolConfig config=new JedisPoolConfig();
        config.setMaxTotal(500);//最大连接数
        config.setMaxIdle(60);//最大空闲时间
        //2.构建Jedis连接池对象
        JedisPool jedisPool=
        new JedisPool(config,"192.168.126.130",6379);
        //3.从池中获取一个连接
        Jedis resource = jedisPool.getResource();
        //4.存取数据测试
        resource.set("maxValue","100");
        System.out.println(resource.get("maxValue"));
        //5.释放资源,将连接还回池中
        resource.close();
        //jedisPool.close(); 关池
    }
}
