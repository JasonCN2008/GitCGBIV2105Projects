package com.jt.redis;

import com.google.gson.Gson;
import org.junit.Test;
import redis.clients.jedis.Jedis;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
public class JedisTests {

    /**
     * Set数据类型测试
     */
    @Test
    public void testSetOper01(){
        //1.建立连接
        Jedis jedis=new Jedis("192.168.126.130",6379);//TCP/IP
        //2.存储数据(不允许重复,且值无序)
        jedis.sadd("set01","A","A","B","B","C");
        //3.删除集合中元素
        jedis.srem("set01","A");
        //4.获取存储的数据
        System.out.println(jedis.scard("set01"));//集合元素的个数
        Set<String> set01 = jedis.smembers("set01");//输出集合中的成员 (点赞的成员)
        System.out.println(set01);
        //5.释放资源
        jedis.close();
    }
    /**
     * 阻塞式队列,brpop/blpop
     */
    @Test
    public void testListOper02(){
        System.out.println(Thread.currentThread().getName());
        //1.建立连接
        Jedis jedis=new Jedis("192.168.126.130",6379);
        //2.存储数据
        jedis.lpush("lst2","A","B","C");
        //3.取数据(队列为空则阻塞)
        jedis.brpop(5,"lst2");
        jedis.brpop(5,"lst2");
        jedis.brpop(5,"lst2");
        jedis.brpop(5,"lst2");//当队列中没有内容时会阻塞
        System.out.println("waiting finish");
        //4.释放资源
        jedis.close();
    }
    /**
     * 测试:redis中list结构的应用
     * 基于FIFO(First In First Out)算法,借助redis实现一个队列
     */
    @Test
    public void testListOper01(){
        //1.建立连接
        Jedis jedis=new Jedis("192.168.126.130",6379);
        //2.存储数据
        jedis.lpush("lst1","A","B","C","C");
        //3.更新数据
        Long pos=jedis.lpos("lst1","A");//获取A元素的位置
        jedis.lset("lst1",pos,"D");//将A元素位置的内容修改为D
        //4.获取数据
        int len=jedis.llen("lst1").intValue();//获取lst1列表中元素个数
        List<String> rpop = jedis.rpop("lst1",len);//获取lst1列表中所有元素
        System.out.println(rpop);
        //5.释放资源
        jedis.close();
    }

    @Test
    public void testHashOper02(){
        //1.建立连接
        Jedis jedis=new Jedis("192.168.126.130",6379);
        //2.存储一篇博客信息
        Map<String,String> map=new HashMap<>();
        map.put("x","100");
        map.put("y","200");
        jedis.hset("point",map);
        //3.获取博客内容并输出
        map=jedis.hgetAll("point");
        System.out.println(map);
        //4.释放资源
        jedis.close();
    }

    /**
     * Hash类型数据的操作实践
     */
    @Test
    public void testHashOper01(){
        //1.建立连接
        Jedis jedis=new Jedis("192.168.126.130",6379);
        //2.基于hash类型存储对象信息
        jedis.hset("member","id","101");
        jedis.hset("member","username","jack");
        jedis.hset("member","mobile","3333333");
        //3.更新hash类型存储的数据
        jedis.hset("member","username","tony");
        //4.获取hash类型数据信息
        String username=jedis.hget("member","username");
        String mobile = jedis.hget("member", "mobile");
        System.out.println(username);
        System.out.println(mobile);
        //5.释放资源
        jedis.close();
    }

    /**
     * 将一个对象(例如map)转换为json格式字符串,然后写入到redis
     */
    @Test
    public void testStringOper02(){
        //1.建立连接
        Jedis jedis=new Jedis("192.168.126.130",6379);
        //2.构建一个map对象,存储一些用户信息
        Map<String,Object> map=new HashMap<>();
        map.put("id",101);
        map.put("username","jack");
        map.put("mobile","11111111111");
        //3.将map对象内容以json字符串结构写入到redis
        Gson gson=new Gson();
        String jsonStr= gson.toJson(map);
        jedis.set("user",jsonStr);
       // jedis.expire("user",3);
        //4.将json字符串内容从redis读出并存储到map对象
        jsonStr=jedis.get("user");
        System.out.println(jsonStr);
        map=gson.fromJson(jsonStr,Map.class);
        System.out.println(map);
        //5.修改map中的数据
        map.put("mobile","2222222222");
        jsonStr= gson.toJson(map);
        jedis.set("user",jsonStr);
        //6.释放资源
        jedis.close();
    }
    /**
     * 测试jedis中对字符串类型的操作
     */
    @Test
    public void testStringOper01() throws InterruptedException {
        //1.建立连接
        Jedis jedis=new Jedis("192.168.126.130",6379);
        //2.存储数据
        jedis.set("id","10001");
        jedis.set("content","hello redis");
        jedis.set("count","10");
        //3.更新数据
        jedis.incr("count");
        jedis.expire("id",1);
        //4.删除指定数据
        jedis.del("content");
        //5.获取数据
        TimeUnit.SECONDS.sleep(1);
        String id=jedis.get("id");
        String content=jedis.get("content");
        String count=jedis.get("count");
        String result=String.format("id=%s,content=%s,count=%s",id,content,count);
        System.out.println(result);
        //6.释放资源
        jedis.close();
    }
    @Test
    public void testGetConnection(){
        Jedis jedis=new Jedis("192.168.126.130",6379);
        String result = jedis.ping();
        System.out.println(result);
    }
}
