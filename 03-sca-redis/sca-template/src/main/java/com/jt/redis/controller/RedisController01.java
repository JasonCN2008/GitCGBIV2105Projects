package com.jt.redis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/redis/demo01/")
public class RedisController01 {
    @Autowired
    private RedisTemplate redisTemplate;
    @PostMapping("{id}")
    public String doInsert(@PathVariable("id") Integer id){
        //insert mysql
        //insert redis
        HashOperations ho = redisTemplate.opsForHash();
        ho.put("blog","id",id);
        return "insert ok";
    }
    @GetMapping("{field}")
    public String doSelect(@PathVariable("field") String field){
        HashOperations ho = redisTemplate.opsForHash();
       return "value is "+ String.valueOf(ho.get("blog","id"));
    }
}
