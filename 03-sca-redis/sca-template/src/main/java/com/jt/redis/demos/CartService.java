package com.jt.redis.demos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class CartService {
    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 向购物车添加数据
     */
    public void addCart(Integer userId,String productId,int num){
        HashOperations hashOperations = redisTemplate.opsForHash();
        //key不存在则会自动创建
        hashOperations.increment("cart:"+userId, productId, num);
    }
    /**
     * 查看购物车商品
     * @param userId
     * @return
     */
    public Map<String,Object> listCart(Integer userId){
        HashOperations hashOperations = redisTemplate.opsForHash();
        return hashOperations.entries("cart:"+userId);
    }

}
