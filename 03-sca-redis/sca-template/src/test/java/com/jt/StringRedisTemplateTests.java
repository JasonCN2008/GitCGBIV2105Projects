package com.jt;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@SpringBootTest
public class StringRedisTemplateTests {

    /**StringRedisTemplate 对象是spring data模块推出的一个用于
     * 操作redis字符串类型数据的一个API对象,底层序列方式使用的是
     * StringRedisSerializer对象
     * */
    @Autowired
    private StringRedisTemplate redisTemplate;
    /**
     * 基于StringRedisTemplate对象操作hash数据类型
     */
    @Test
    void testHashOper01(){
        //1.获取操作hash类型数据的对象
        HashOperations<String, Object, Object> ho
                = redisTemplate.opsForHash();
        //2.操作hash类型数据
        ho.put("user","id","100");
        ho.put("user","username","jack");
        ho.put("user","state","true");
        List<Object> user = ho.values("user");
        System.out.println(user);
    }

    /**以json格式存储一个对象到redis数据库
     * 提示:不能使用Gson,可以使用jackson*/
    @Test
    void testStringOper02() throws JsonProcessingException {
        //1.创建一个map,用于存储对象数据
        Map<String,String> map=new HashMap<>();
        map.put("id","101");
        map.put("username","jack");
        //2.将map转换为json格式字符串
        String jsonStr =
        new ObjectMapper().writeValueAsString(map);
        //3.将json字符串借助StringRedisTemplate存储到redis,并读取
        ValueOperations<String, String> vo =
                redisTemplate.opsForValue();
        vo.set("userJsonStr",jsonStr);
        jsonStr=vo.get("userJsonStr");
        map= new ObjectMapper().readValue(jsonStr,Map.class);
        System.out.println(map);
    }
    /**
     * 字符串数据基本操作实践
     * @throws InterruptedException
     */
    @Test
    void testStringOper01() throws InterruptedException {
       //1.获取一个操作redis字符串的值对象
        ValueOperations<String, String> vo =
                redisTemplate.opsForValue();
        //2.基于这个值对象操作redis中的字符串数据
        vo.set("x","100");
        vo.increment("x");
        vo.set("y","200",1, TimeUnit.SECONDS);//设置key的失效时间
        TimeUnit.SECONDS.sleep(1);//阻塞1秒
        String x = vo.get("x");
        System.out.println(x);
        String y=vo.get("y");
        System.out.println(y);
    }

    @Test
    void testConnection(){
        RedisConnection connection =
                redisTemplate.getConnectionFactory()
                        .getConnection();
        String ping = connection.ping();
        System.out.println(ping);
    }


}
